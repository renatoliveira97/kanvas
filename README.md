<div align="center">
    <h1>Kanvas</h1>
    <h4>Backend service API for Kanvas app<h4>
    <img src="https://blog-geek-midia.s3.amazonaws.com/wp-content/uploads/2020/08/13110931/django-framework.png" alt="Django image">
</div>

## Setup

* Clone the repository & `cd` to the root directory.

* Run the command bellow to create an virtual environment:

```
python -m venv venv
```

* Install the project dependencies:

```
pip install -r requirements.txt
```

## Run

* To run the application, run the migrations:

```
python manage.py migrate
```

* Then run the server:

```
python manage.py runserver
```

The application will run locally in port `8000`.

## Run Tests

To run all the application tests, you need to use the command bellow:

```
python manage.py test -v 2
```

## Remarks

This template is developed and tested on

- Python v3.9.5
- Ubuntu 20.04.3 LTS
