from rest_framework import status
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from address.models import Address
from address.serializers import AddressSerializer
from users.models import User

class AddressView(APIView):

    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def put(self, request):

        REQUIRED_FIELDS = ('street', 'house_number', 'city', 'state', 'zip_code', 'country')
        missing_fields = []

        for value in REQUIRED_FIELDS:
            try:
                request.data[value]
            except:
                missing_fields.append(value)

        missing_fields_obj = {}

        for value in missing_fields:
            missing_fields_obj[value] = "Missing parameter"    

        if missing_fields_obj != {}:
            return Response(missing_fields_obj ,status=status.HTTP_400_BAD_REQUEST)

        user_address = None
        address_exists = False

        for address in Address.objects.filter(zip_code=request.data['zip_code']):
            if address.house_number == request.data['house_number']:
                user_address = address
                address_exists = True

        if address_exists:
            user_uuid = request.user.uuid
            update_data = {"address": user_address}
            User.objects.filter(uuid=user_uuid).update(**update_data)
            address = user_address
        else:
            address = Address.objects.create(
                street = request.data['street'],
                house_number = request.data['house_number'],
                city = request.data['city'],
                state = request.data['state'],
                zip_code = request.data['zip_code'],
                country = request.data['country']
            )

        serialized = AddressSerializer(address)

        return Response(serialized.data, status=status.HTTP_200_OK)
