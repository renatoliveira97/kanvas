
from rest_framework import status
from rest_framework.authentication import authenticate, TokenAuthentication
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from rest_framework.views import APIView

from users.models import User
from users.permissions import IsInstructor

from .serializers import UserSerializer


# Create your views here.
class UserView(APIView):

    authentication_classes = [TokenAuthentication]
    permission_classes = [IsInstructor]

    def post(self, request):

        REQUIRED_FIELDS = ('first_name', 'last_name', 'password', 'email', 'is_admin')
        missing_fields = []

        for value in REQUIRED_FIELDS:
            try:
                request.data[value]
            except:
                missing_fields.append(value)

        missing_fields_obj = {}

        for value in missing_fields:
            missing_fields_obj[value] = "Missing parameter"    

        if missing_fields_obj != {}:
            return Response(missing_fields_obj ,status=status.HTTP_400_BAD_REQUEST)

        try:
            User.objects.get(email=request.data['email'])
            return Response({"message": "User already exists"}, status=status.HTTP_422_UNPROCESSABLE_ENTITY)
        except:
            user = User.objects.create_user(
                first_name = request.data['first_name'],
                last_name = request.data['last_name'],
                password = request.data['password'],
                email = request.data['email'],
                is_admin = request.data['is_admin']
            )

            serialized = UserSerializer(user)

            return Response(serialized.data, status=status.HTTP_201_CREATED)

    def get(self, request):
        
        users = User.objects.all()

        serialized = UserSerializer(users, many=True)

        return Response(serialized.data, status=status.HTTP_200_OK)

class UserLogin(APIView):

    def post(self, request):

        try:
            email = request.data['email']
        except KeyError:
            return Response({
                "email": "Missing parameter",
            }, status=status.HTTP_400_BAD_REQUEST)

        try:
            password = request.data['password']
        except KeyError:
            return Response({
                "password": "Missing parameter",
            }, status=status.HTTP_400_BAD_REQUEST)

        user = authenticate(email=email, password=password)

        if user:
            token = Token.objects.get_or_create(user=user)[0]
            return Response({'token': token.key}, status=status.HTTP_200_OK)

        return Response({'message':'Invalid Credentials'}, status=status.HTTP_401_UNAUTHORIZED)

