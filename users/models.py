from django.utils import timezone
from django.db import models
from django.contrib.auth.models import AbstractUser, BaseUserManager
import uuid

class CustomUserManager(BaseUserManager):

    def _create_user(self, email, password, is_superuser, **extra_fields):

        now = timezone.now()
        
        if not email:
            raise ValueError('The given email must be set')
        
        email = self.normalize_email(email)
        
        user = self.model(email=email, is_active=True,
                          is_superuser=is_superuser, last_login=now,
                          date_joined=now, **extra_fields)

        user.set_password(password)

        user.save(using=self._db)

        return user
        
    def create_user(self, email, password=None, **extra_fields):
        return self._create_user(email, password, True,
                                         **extra_fields)
        
    def create_superuser(self, email, password, **extra_fields):
        return self._create_user(email, password, True,
                                     **extra_fields)

class User(AbstractUser):

    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

    is_admin = models.BooleanField()

    email = models.EmailField(max_length=255, unique=True)

    username = models.CharField(unique=False, null=True, max_length=255)

    address = models.ForeignKey('address.Address', null=True, on_delete=models.PROTECT, related_name='users')

    USERNAME_FIELD = 'email'

    REQUIRED_FIELDS = []

    objects = CustomUserManager()
