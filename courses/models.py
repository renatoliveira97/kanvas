from django.db import models
import uuid

class Course(models.Model):

    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

    name = models.CharField(unique=True, max_length=255)

    demo_time = models.TimeField()

    created_at = models.DateTimeField()

    link_repo = models.CharField(max_length=255)

    instructor = models.OneToOneField('users.User', related_name='instructor_at', null=True, on_delete=models.PROTECT)

    students = models.ManyToManyField('users.User', related_name='courses')
