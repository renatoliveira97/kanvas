from datetime import datetime
from rest_framework import status
from rest_framework.authentication import TokenAuthentication
from rest_framework.response import Response
from rest_framework.views import APIView

from users.models import User

from courses.models import Course
from courses.permissions import IsInstructor
from courses.serializers import CourseSerializer

class CourseView(APIView):

    authentication_classes = [TokenAuthentication]
    permission_classes = [IsInstructor]

    def post(self, request):

        REQUIRED_FIELDS = ('name', 'demo_time', 'link_repo')
        missing_fields = []

        for value in REQUIRED_FIELDS:
            try:
                request.data[value]
            except:
                missing_fields.append(value)

        missing_fields_obj = {}

        for value in missing_fields:
            missing_fields_obj[value] = "Missing parameter"    

        if missing_fields_obj != {}:
            return Response(missing_fields_obj ,status=status.HTTP_400_BAD_REQUEST)

        try:
            Course.objects.get(name=request.data['name'])
            return Response({"message": "Course already exists"}, status=status.HTTP_422_UNPROCESSABLE_ENTITY)
        except:
            course = Course.objects.create(
                name = request.data['name'],
                demo_time = request.data['demo_time'],
                link_repo = request.data['link_repo'],
                created_at = datetime.now()
            )

            serialized = CourseSerializer(course)

            return Response(serialized.data, status=status.HTTP_201_CREATED)

    def get(self, request):
        
        courses = Course.objects.all()

        serialized = CourseSerializer(courses, many=True)

        return Response(serialized.data, status=status.HTTP_200_OK)

class CourseByIdView(APIView):

    authentication_classes = [TokenAuthentication]
    permission_classes = [IsInstructor]

    def get(self, request, course_id=''):

        try:

            course = Course.objects.get(uuid=course_id)
            
            serialized = CourseSerializer(course)

            return Response(serialized.data)

        except:

            return Response({"message": "Course does not exist"}, status=status.HTTP_404_NOT_FOUND)

    def patch(self, request, course_id=''):

        try:
            Course.objects.get(uuid=course_id)
        except:
            return Response({"message": "Course does not exist"}, status=status.HTTP_404_NOT_FOUND)

        try:

            name = request.data['name']

            Course.objects.get(name=name)

            return Response({'message': 'This course name already exists'}, status=status.HTTP_422_UNPROCESSABLE_ENTITY)

        except:

            VALID_FIELDS = ('name', 'demo_time', 'link_repo')

            for value in request.data:
                if value not in VALID_FIELDS:
                    return Response({'message':'Invalid data.'}, status=status.HTTP_400_BAD_REQUEST)

            Course.objects.filter(uuid=course_id).update(**request.data)

            course = Course.objects.get(uuid=course_id)

            serialized = CourseSerializer(course)

            return Response(serialized.data, status=status.HTTP_200_OK)

    def delete(self, request, course_id=''):

        try:

            course = Course.objects.get(uuid=course_id)
            
            course.delete()

            return Response(status=status.HTTP_204_NO_CONTENT)
        
        except:

            return Response({"message": "Course does not exist"}, status=status.HTTP_404_NOT_FOUND)

class InsertInstructorInACourseView(APIView):

    authentication_classes = [TokenAuthentication]
    permission_classes = [IsInstructor]

    def put(self, request, course_id=''):

        try:
            Course.objects.get(uuid=course_id)
        except:
            return Response({"message": "Course does not exist"}, status=status.HTTP_404_NOT_FOUND)

        try:
            instructor_id = request.data['instructor_id']
        except:
            return Response({"instructor_id": "Missing parameter"}, status=status.HTTP_400_BAD_REQUEST)

        try:
            instructor = User.objects.get(uuid=instructor_id)

            if instructor.is_admin == False:
                return Response({"message": "Instructor id does not belong to an admin"}, status=status.HTTP_422_UNPROCESSABLE_ENTITY)

        except:
            return Response({"message": "Invalid instructor_id"}, status=status.HTTP_404_NOT_FOUND)

        update_data = {"instructor": instructor}

        Course.objects.filter(instructor=instructor).update(instructor=None)

        Course.objects.filter(uuid=course_id).update(**update_data)

        course = Course.objects.get(uuid=course_id)

        serialized = CourseSerializer(course)

        return Response(serialized.data, status=status.HTTP_200_OK)

class InsertStudentsInACourseView(APIView):

    authentication_classes = [TokenAuthentication]
    permission_classes = [IsInstructor]

    def put(self, request, course_id=''):

        try:
            Course.objects.get(uuid=course_id)
        except:
            return Response({"message": "Course does not exist"}, status=status.HTTP_404_NOT_FOUND)

        try:
            students_id = request.data['students_id']
            if not isinstance(students_id, list):
                return Response({"students_id": "Invalid type. The input needs to be a list."}, status=status.HTTP_400_BAD_REQUEST)
        except:
            return Response({"students_id": "Missing parameter"}, status=status.HTTP_400_BAD_REQUEST)

        try:
            students = []
            for student_id in request.data['students_id']:
                student = User.objects.get(uuid=student_id)
                if student.is_admin == True:
                    return Response({'message': 'Some student id belongs to an Instructor'}, status=status.HTTP_422_UNPROCESSABLE_ENTITY)
                students.append(student)
        except:
            return Response({"message": "Invalid students_id list"}, status=status.HTTP_404_NOT_FOUND)

        courses = Course.objects.filter(uuid=course_id)

        for course in courses:
            course.students.clear()
            for student in students:
                course.students.add(student)

        course = Course.objects.get(uuid=course_id)

        serialized = CourseSerializer(course)

        return Response(serialized.data, status=status.HTTP_200_OK)
