from django.urls import path

from courses.views import CourseByIdView, CourseView, InsertInstructorInACourseView, InsertStudentsInACourseView

urlpatterns = [
    path('courses/', CourseView.as_view()),
    path('courses/<str:course_id>/', CourseByIdView.as_view()),
    path('courses/<str:course_id>/registrations/instructor/', InsertInstructorInACourseView.as_view()),
    path('courses/<str:course_id>/registrations/students/', InsertStudentsInACourseView.as_view())
]
